"""Module to test save_bids_curation.py"""

import logging
from pathlib import Path
from unittest import TestCase
from unittest.mock import patch

from flywheel_bids.supporting_files.BIDSCuration import BIDSCuration

import scripts.save_bids_curation as sbc

log = logging.getLogger(__name__)


class Args:
    def __init__(self):
        self.pickle = True
        self.verbose = 1
        self.intended_for_regexes = ""
        # self.intended_for_regexes = ".*fmap(_|-)SE(_|-).* _run-1 .*gre-.* _run-2"


@patch("scripts.save_bids_curation.get_bids_info", return_value=(2, 3, 0))
def test_save(mock_get_bids_info, install_gear, capfd, search_sysout, print_captured):
    """."""

    TestCase.skipTest("", "Can't find project.")

    install_gear("upenn_pickle.zip")

    bc = BIDSCuration()

    args = Args()

    sbc.main(bc, args, None, "epsteinlab_brainSLAM", None)

    captured = capfd.readouterr()
    print_captured(captured)

    output_files = [
        "epsteinlab_brainSLAM_acquisitions.csv",
        "epsteinlab_brainSLAM_acquisitions_details_1.csv",
        "epsteinlab_brainSLAM_acquisitions_details_2.csv",
        "epsteinlab_brainSLAM_intendedfors.csv",
        "epsteinlab_brainSLAM_niftis.csv",
    ]
    for path in output_files:
        assert Path(path).exists()
    assert search_sysout(captured, "1 duplicate BIDS paths were detected")
