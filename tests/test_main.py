"""Module to test main.py"""

import json
import logging
from pathlib import Path
from unittest.mock import patch

import flywheel

from run import main

log = logging.getLogger(__name__)

config = {
    "config": {
        "base_template": "BIDS-v1",
        "entire_project": True,
        "reset": True,
        "use_or_save_config": "",
        "verbose": "DEBUG",
    },
    "inputs": {
        "api_key": {"key": "rollout.ce.flywheel.io:yadayadayadayadaHO"},
        "template": {
            "base": "file",
            "hierarchy": {"type": "project", "id": "609ed1a75418fa7f6dd82a6a"},
            "location": {
                "name": "levitas-tutorial-project-template.json",
                "path": "/flywheel/v0/input/template/levitas-tutorial-project-template.json",
            },
            "object": {
                "info": {},
                "tags": [],
                "measurements": [],
                "classification": {},
                "mimetype": "application/json",
                "type": "source code",
                "modality": None,
                "size": 5709,
            },
        },
    },
    "destination": {"type": "project", "id": "609ed1a75418fa7f6dd82a6a"},
}


class Acquisition:
    def __init__(self):
        self.label = "TheAcquisitionLabel"


class Session:
    def __init__(self):
        self.label = "TheSessionLabel"


class Subject:
    def __init__(self):
        self.label = "TheSubjectCode"

    def get_file(self, name):
        return None


class Project:
    def __init__(self):
        self.label = "TheProjectLabel"
        self.subjects = []

    def get_file(self, file_name):
        return False


class Parent:
    def __init__(self, level):
        self.type = level


class Destination:
    def __init__(self, level):
        self.parent = Parent(level)
        self.parents = {
            "group": "monkeyshine",
            "project": "proj_id",
            "subject": "subj_id",
            "session": "sess_id",
            "acquisition": "acq_id",
        }
        self.container_type = "analysis"

    def get(self, level, none):
        return self.parent


class FW:
    def __init__(self, level):
        self.destination = Destination(level)

    def get_project(self, id):
        return Project()

    def get_subject(self, id):
        return Subject()

    def get(self, id):
        if id == "01234":
            ret = self.destination
        elif id == "proj_id":
            ret = Project()
        elif id == "subj_id":
            ret = Subject()
        elif id == "sess_id":
            ret = Session()
        elif id == "acq_id":
            ret = Acquisition()
        else:
            ret = self.destination
        return ret


@patch("run.get_bids_info", return_value=(2, 3, 0))
@patch(
    "flywheel_bids.curate_bids.utils.get_project_id_from_subject_id",
    return_value="0123",
)
@patch("flywheel_bids.curate_bids.curate_bids")
def test_main(mock_method1, mock_method2, mock_method3):
    """."""

    with open("/flywheel/v0/config.json", "w") as fp:
        json.dump(config, fp)

    with patch("flywheel.Client", return_value=FW("subject")):

        # run.get_bids_info = MagicMock(return_value=(2, 3, 0))

        Path("output").mkdir(exist_ok=True)

        with flywheel.GearContext() as gear_context:
            main(gear_context)

        assert flywheel.Client.called
        assert mock_method1.called
        assert mock_method2.called


@patch("run.get_bids_info", return_value=(2, 3, 0))
@patch("run.main_with_args")
def test_main_again(mock_main_with_args, mock_get_bids_info):
    """."""

    with open("/flywheel/v0/config.json", "w") as fp:
        json.dump(config, fp)

    with patch("flywheel.Client", return_value=FW("subject")):

        with flywheel.GearContext() as gear_context:
            main(gear_context)

    mock_main_with_args.assert_called_with(
        "rollout.ce.flywheel.io:yadayadayadayadaHO",
        "sess_id",
        True,
        False,
        "BIDS-v1",
        "/flywheel/v0/input/template/levitas-tutorial-project-template.json",
        subject_id="subj_id",
        verbosity=0,
        pickle_tree=False,
        dry_run=False,
    )
