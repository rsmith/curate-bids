#! /usr/bin/env python3
"""Run curate-bids on project "ReproIn"

    This script was created to run Job ID 61660786af0d9b1c452d6d76
    In project "bids-curation-test/ReproIn"
    On Flywheel Instance https://ga.ce.flywheel.io/api
"""

import argparse
import os
from datetime import datetime

import flywheel

input_files = {}


def main(fw):

    gear = fw.lookup("gears/curate-bids")
    print("gear.gear.version for job was = 2.1.1_1.0.1")
    print(f"gear.gear.version now = {gear.gear.version}")
    print("destination_id = 613ba1c2e8a08e01b0172987")
    print("destination type is: project")
    destination = fw.lookup("bids-curation-test/ReproIn")

    inputs = dict()
    for key, val in input_files.items():
        container = fw.lookup(val["container_path"])
        inputs[key] = container.get_file(val["location_name"])

    config = {
        "base_template": "ReproIn",
        "intendedfor_regexes": "",
        "reset": True,
        "use_or_save_config": "",
        "verbosity": "DEBUG",
    }

    now = datetime.now()
    analysis_label = (
        f'{gear.gear.name} {now.strftime("%m-%d-%Y %H:%M:%S")} SDK launched'
    )
    print(f"analysis_label = {analysis_label}")

    analysis_id = gear.run(
        analysis_label=analysis_label,
        config=config,
        inputs=inputs,
        destination=destination,
    )
    print(f"analysis_id = {analysis_id}")
    return analysis_id


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description=__doc__)
    args = parser.parse_args()

    fw = flywheel.Client("")
    print(fw.get_config().site.api_url)

    analysis_id = main(fw)

    os.sys.exit(0)
