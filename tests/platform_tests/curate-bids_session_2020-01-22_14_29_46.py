#! /usr/bin/env python3
"""Run curate-bids on session "2020-01-22 14_29_46"

    This script was created to run Job ID 61560a30dec526d476eac931
    In project "bids-curation-test/Levitas_Tutorial"
    On Flywheel Instance https://rollout.ce.flywheel.io/api
"""

import argparse
import os
from datetime import datetime

import flywheel

input_files = {
    "template": {
        "container_path": "bids-curation-test/Levitas_Tutorial",
        "location_name": "levitas-tutorial-project-template.json",
    }
}


def main(fw):

    gear = fw.lookup("gears/curate-bids")
    print("gear.gear.version for job was = 2.0.14_0.9.3_rc18")
    print(f"gear.gear.version now = {gear.gear.version}")
    print("destination_id = 60d9f5b2cdd96dd507e924a9")
    print("destination type is: session")
    destination = fw.lookup(
        "bids-curation-test/Levitas_Tutorial/10462@thwjames_OpenScience/2020-01-22 14_29_46"
    )

    inputs = dict()
    for key, val in input_files.items():
        container = fw.lookup(val["container_path"])
        inputs[key] = container.get_file(val["location_name"])

    config = {
        "base_template": "ReproIn",
        "entire_project": True,
        "intendedfor_regexes": ".*fmap(_|-)SE(_|-).* _run-1 .*gre-.* _run-2",
        "reset": True,
        "use_or_save_config": "Ignore Config File",
        "verbose": "DEBUG",
        "verbosity": "INFO",
    }

    now = datetime.now()
    analysis_label = (
        f'{gear.gear.name} {now.strftime("%m-%d-%Y %H:%M:%S")} SDK launched'
    )
    print(f"analysis_label = {analysis_label}")

    analysis_id = gear.run(
        analysis_label=analysis_label,
        config=config,
        inputs=inputs,
        destination=destination,
    )
    print(f"analysis_id = {analysis_id}")
    return analysis_id


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description=__doc__)
    args = parser.parse_args()

    fw = flywheel.Client("")
    print(fw.get_config().site.api_url)

    analysis_id = main(fw)

    os.sys.exit(0)
