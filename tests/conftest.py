import os
import shutil
from pathlib import Path
from zipfile import ZipFile

import pytest

os.chdir("/flywheel/v0")
FWV0 = Path.cwd()
ZIPPED_GEARS = "tests/zipped_gears"


@pytest.fixture
def install_gear():
    def _method(zip_name):
        """unarchive initial gear to simulate running inside a real gear.

        This will delete and then install: config.json input/ output/ work/ freesurfer/

        Args:
            zip_name (str): name of zip file that holds simulated gear.
        """
        test_path = Path("/src/" + ZIPPED_GEARS)
        if not test_path.exists():  # fix for running in circleci
            test_path = FWV0 / ZIPPED_GEARS

        print("\nRemoving previous gear...")

        old_files = list(FWV0.glob("*.pickle"))
        old_files.append(FWV0 / "config.json")
        for file_path in old_files:
            if file_path.exists():
                print(f"unlink {str(file_path)}")
                file_path.unlink()

        for dir_name in ["input", "output", "work"]:
            path = Path(FWV0 / dir_name)
            if path.exists():
                print(f"shutil.rmtree({str(path)})")
                shutil.rmtree(path)

        print(f'\ninstalling new gear, "{zip_name}" at {str(FWV0)}')
        zipfile_path = test_path / zip_name
        input_zip = ZipFile(zipfile_path, "r")
        print("Unzipping file,", zipfile_path)
        input_zip.extractall(str(FWV0))
        # os.system("ls -l " + str(FWV0))

    return _method


@pytest.fixture
def print_captured():
    def _method(captured):
        """Show what has been captured in std out and err."""

        print("\nout")
        for ii, msg in enumerate(captured.out.split("\n")):
            print(f"{ii:2d} {msg}")
        print("\nerr")
        for ii, msg in enumerate(captured.err.split("\n")):
            print(f"{ii:2d} {msg}")

    return _method


@pytest.fixture
def search_stdout_contains():
    def _method(captured, find_me, contains_me):
        """Search stdout message for find_me, return true if it contains contains_me"""

        for msg in captured.out.split("/n"):
            if find_me in msg:
                if contains_me in msg:
                    return True
        return False

    return _method


@pytest.fixture
def search_sysout():
    def _method(captured, find_me):
        """Search capsys message for find_me, return message"""

        for msg in captured.out.split("/n"):
            if find_me in msg:
                return msg
        return ""

    return _method


@pytest.fixture
def search_syserr():
    def _method(captured, find_me):
        """Search capsys message for find_me, return message"""

        for msg in captured.err.split("\n"):
            if find_me in msg:
                return msg
        return ""

    return _method


@pytest.fixture
def print_caplog():
    def _method(caplog):
        """Show what has been captured in the log."""

        print("\nmessages")
        for ii, msg in enumerate(caplog.messages):
            print(f"{ii:2d} {msg}")
        print("\nrecords")
        for ii, rec in enumerate(caplog.records):
            print(f"{ii:2d} {rec}")

    return _method


@pytest.fixture
def search_caplog():
    def _method(caplog, find_me):
        """Search caplog message for find_me, return message"""

        for msg in caplog.messages:
            if find_me in msg:
                return msg
        return ""

    return _method


@pytest.fixture
def search_caplog_contains():
    def _method(caplog, find_me, contains_me):
        """Search caplog message for find_me, return true if it contains contains_me"""

        for msg in caplog.messages:
            if find_me in msg:
                if contains_me in msg:
                    return True
        return False

    return _method
