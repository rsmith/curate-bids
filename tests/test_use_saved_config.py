"""Module to test main.py"""

import json
import logging
from pathlib import Path
from unittest import TestCase
from unittest.mock import patch

import flywheel

from run import main

log = logging.getLogger(__name__)

# "api_key": {"key": "rollout.ce.flywheel.io:yadayadayadayadaHO"},
config = {
    "config": {
        "base_template": "BIDS-v1",
        "intendedfor_regexes": ".*fmap(_|-)SE(_|-).* _run-1 .*gre-.* _run-2",
        "entire_project": True,
        "reset": True,
        "use_or_save_config": "",
        "verbosity": "DEBUG",
    },
    "inputs": {
        "template": {
            "base": "file",
            "hierarchy": {"type": "project", "id": "60d9f593cdd96dd507e924a4"},
            "location": {
                "name": "levitas-tutorial-project-template.json",
                "path": "/flywheel/v0/input/template/levitas-tutorial-project-template.json",
            },
            "object": {
                "info": {},
                "tags": [],
                "measurements": [],
                "classification": {},
                "mimetype": "application/json",
                "type": "source code",
                "modality": None,
                "size": 5709,
            },
        },
    },
    "destination": {"type": "analysis", "id": "614de498e93f5d814ceaca6a"},
}
# Destination is an analysis on a project


@patch("run.main_with_args")
def test_use_or_save_config_save(mock_main_with_args):
    """."""

    user_json = Path(Path.home() / ".config/flywheel/user.json")
    if not user_json.exists():
        TestCase.skipTest("", f"No API key available in {str(user_json)}")
    with open(user_json) as json_file:
        data = json.load(json_file)
        if "rollout" not in data["key"]:
            TestCase.skipTest("", "Not logged in to rollout.")

    config["config"]["use_or_save_config"] = "Save Config File"

    with open("/flywheel/v0/config.json", "w") as fp:
        json.dump(config, fp)

    Path("output").mkdir(exist_ok=True)

    with flywheel.GearContext() as gear_context:
        main(gear_context)

    assert mock_main_with_args.called
    assert (
        gear_context.config["intendedfor_regexes"]
        == ".*fmap(_|-)SE(_|-).* _run-1 .*gre-.* _run-2"
    )


@patch("run.main_with_args")
def test_use_or_save_config_empty(mock_main_with_args):
    """."""

    user_json = Path(Path.home() / ".config/flywheel/user.json")
    if not user_json.exists():
        TestCase.skipTest("", f"No API key available in {str(user_json)}")
    with open(user_json) as json_file:
        data = json.load(json_file)
        if "rollout" not in data["key"]:
            TestCase.skipTest("", "Not logged in to rollout.")

    config["config"]["use_or_save_config"] = ""

    with open("/flywheel/v0/config.json", "w") as fp:
        json.dump(config, fp)

    Path("output").mkdir(exist_ok=True)

    with flywheel.GearContext() as gear_context:
        main(gear_context)

    assert not mock_main_with_args.called


@patch("run.main_with_args")
def test_use_or_save_config_stop(mock_main_with_args):
    """."""

    user_json = Path(Path.home() / ".config/flywheel/user.json")
    if not user_json.exists():
        TestCase.skipTest("", f"No API key available in {str(user_json)}")
    with open(user_json) as json_file:
        data = json.load(json_file)
        if "rollout" not in data["key"]:
            TestCase.skipTest("", "Not logged in to rollout.")

    config["config"]["intendedfor_regexes"] = ""
    config["config"]["use_or_save_config"] = "Disable Config File"

    with open("/flywheel/v0/config.json", "w") as fp:
        json.dump(config, fp)

    Path("output").mkdir(exist_ok=True)

    with flywheel.GearContext() as gear_context:
        main(gear_context)

    assert mock_main_with_args.called == False
    assert gear_context.config["intendedfor_regexes"] == ""
