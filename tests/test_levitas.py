"""Module to test main.py"""

import glob
import json
import logging
from pathlib import Path
from unittest import TestCase

import flywheel

from run import main

log = logging.getLogger(__name__)


def test_levitas_works(install_gear, caplog, search_caplog):
    """Curate the Levitas Tutorial session 60d9f5b2cdd96dd507e924a9 on rollout."""

    caplog.set_level(logging.DEBUG)

    user_json = Path(Path.home() / ".config/flywheel/user.json")
    if not user_json.exists():
        TestCase.skipTest("", f"No API key available in {str(user_json)}")
    with open(user_json) as json_file:
        data = json.load(json_file)
        if "rollout" not in data["key"]:
            TestCase.skipTest("", "Not logged in to rollout.")

    install_gear("levitas.zip")

    with flywheel.GearContext() as gear_context:
        main(gear_context)

    assert search_caplog(caplog, "Updating BIDS metadata on Flywheel")
    assert search_caplog(caplog, "No duplicates were found")
    assert search_caplog(caplog, "Curated 1 session containers")
    assert search_caplog(caplog, "Curated 19 acquisition containers")

    ls = glob.glob("output/*.csv")
    assert len(ls) == 5
    assert "output/bids-curation-test_Levitas_Tutorial_niftis.csv" in ls
