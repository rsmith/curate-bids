import json
import logging
from datetime import datetime

import flywheel
from flywheel_bids.curate_bids import main_with_args
from flywheel_bids.supporting_files.BIDSCuration import BIDSCuration
from flywheel_bids.supporting_files.get_bids import get_bids_info

from scripts.save_bids_curation import make_file_name_safe

log = logging.getLogger(__name__)

CONFIG_FILE_NAME_PRE = "curate-bids-"  # + project  subject  or  session
CONFIG_FILE_NAME_POST = "-config.json"


def attach_config_to(
    gear_context, template, project, config_file_name, okay_to_use_saved_config
):

    data = dict()
    data["okay_to_use"] = okay_to_use_saved_config
    data["config"] = dict()

    keys = ["base_template", "intendedfor_regexes", "reset", "verbosity"]
    data["config"] = {key: gear_context.config.get(key) for key in keys}

    if template is not None:
        data["inputs"] = {
            "template": {
                "hierarchy_id": template["hierarchy"]["id"],
                "file_name": template["location"]["name"],
            }
        }
    else:
        data["inputs"] = {}

    file_spec = flywheel.FileSpec(config_file_name, json.dumps(data), "text/plain")
    project.upload_file(file_spec)
    log.info(
        "Attached %s to project '%s'.  okay_to_use_saved_config is %s",
        config_file_name,
        project.label,
        okay_to_use_saved_config,
    )


def run_job_with_saved_config(fw, config, project, container, config_file_name, job):
    """Launch a new job with the saved config and input file (if provided).

    Note: The "saved config" is those four config values plus a possible input json file.  The goal
    is to let the user not do anything but click the "Run Gear" button If they don't provide that file,
    the gear engine won't have it ready for this job, so a new job has to be launched to get that input file.

    Args:
        fw: Flywheel Client:
        config:  The current gear config.json
        project: The full project object
        container: The full project, subject, or session that the new analysis will be attached to
        config_file_name: The name of the file that will hold config/input information attached to the project
        job:  The current job so the ID can be put in the name of the new analysis job

    Returns:
        True if a job was not launched (disabeled config file) and so it still needs to be run

    """
    data = project.read_file(config_file_name)
    read_config = json.loads(data)

    if read_config["okay_to_use"]:  # turn this feature off by setting this to False

        config.update(read_config["config"])

        # without this, jobs will be spawned forever to do nothing:
        config["use_or_save_config"] = "Ignore Config File"

        inputs = dict()
        for key, val in read_config["inputs"].items():
            inputs_container = fw.get(val["hierarchy_id"])
            inputs[key] = inputs_container.get_file(val["file_name"])

        gear = fw.lookup("gears/curate-bids")

        now = datetime.now()
        analysis_label = (
            f'curate-bids {now.strftime("%m-%d-%Y %H:%M:%S")} launched by job {job.id}'
        )

        analysis_id = gear.run(
            analysis_label=analysis_label,
            config=config,
            inputs=inputs,
            destination=container,
        )
        log.info("Launched %s id=%s", analysis_label, analysis_id)
        return False  # job launched, does not still need to be run

    else:
        log.info(
            "Found disabled saved config file.  Ignoring it and using currently supplied input and config. "
        )
        return True  # job sill needs to be launched because it wasn't done here


def main(gear_context):

    # gear_context.config.get("verbosity") can be "INFO","DEBUG"
    verbosity = 0
    if gear_context.config.get("verbosity") == "DEBUG":
        verbosity = 1

    if verbosity == 0:
        print('setting log level to "info"')
        logging.basicConfig(
            format="[ %(module)s %(asctime)2s %(levelname)2s] %(message)s"
        )
        log.setLevel(logging.INFO)

    elif verbosity == 1:
        print('setting log level to "debug"')
        logging.basicConfig(
            format="[ %(module)s %(asctime)2s %(levelname)2s: %(lineno)s] %(message)s"
        )
        log.setLevel(logging.DEBUG)

    # First look at context that isn't saved in curate-bids-config.json
    api_input = gear_context.get_input("api_key")
    if api_input:
        api_key = gear_context.get_input("api_key")["key"]
    else:
        api_key = ""  # attempt to get api-key from CLI (for local testing)

    template = gear_context.get_input("template")
    if template:
        template_file = template["location"]["path"]
        log.info(
            "Input project curation template is '%s'", template["location"]["name"]
        )
    else:
        template_file = None

    destination_id = gear_context.destination["id"]
    pickle_tree = gear_context.config.get("pickle_tree", False)
    dry_run = gear_context.config.get("dry_run", False)
    reset = gear_context.config.get("reset")
    template_type = gear_context.config.get("base_template")
    intended_for_regexes = gear_context.config.get("intendedfor_regexes")
    use_or_save_config = gear_context.config.get("use_or_save_config")

    if use_or_save_config == "Save Config File":
        # Save some values from current config.json as 'curate-bids-<run-level>-config.json' on
        # the project so it will be used by default (the last else below)
        save_config = True
        use_saved_config = False
        okay_to_use_saved_config = True
        log.info("Saving Config")
    elif use_or_save_config == "Ignore Config File":
        # Ignore config file and use the current config
        save_config = False
        use_saved_config = False
        okay_to_use_saved_config = True
        log.info("Using current config, ignoring saved Config if present")
    elif use_or_save_config == "Disable Config File":
        save_config = True
        use_saved_config = False
        okay_to_use_saved_config = False
        log.info("Disabling saved Config file")
    elif (
        use_or_save_config == ""
    ):  # use_or_save_config is not set.  This is the default
        # Check for 'curate-bids-config.json' at the run level and use it if present
        save_config = False
        use_saved_config = True
        okay_to_use_saved_config = True
        log.info("Using Saved Config if present")
    else:
        raise ValueError("Unknown value for use_or_save_config: %s", use_or_save_config)

    fw = flywheel.Client(api_key)

    destination = fw.get(destination_id)

    project = fw.get_project(destination.parents["project"])

    # The use of session_only and session_id here is confusing to maintain backwards compatibility.
    # Ideally, subject_id would be set if curating a specific subject and session_id would be set if curating a
    # specific session.  But the function main_with_args() expected to find the project using a session ID.  If
    # session_only is True, only the session should be curated but if it is false, the entire_project should be
    # curated.  Previously entire_project was a config parameter, but now the gear must be run at the proper
    # level (project, subject, session) to curate that level.  Now these variables are set to achieve the desired
    # behavior of running the gear at the user-specified level.  subject_id was added as a kwarg to main_with_args()
    # to be able to run at the subject level and if it is set, main_with_args() will find the project using the
    # subject_id.  For now, assume the gear is running at a particular session:
    session_only = True
    subject_id = ""
    session_id = destination.parents["session"]

    # --- Set up for run ---
    if destination.parent.type == "project":  # subject_id will not be used
        session = (
            project.sessions.find_first()
        )  # just get one so the project id can be found
        session_id = session.id
        session_only = False  # session_id will not be used to curate only the session
        log.info("Curating entire project")
        config_file_name = CONFIG_FILE_NAME_PRE + "project" + CONFIG_FILE_NAME_POST
        container = project

    elif destination.parent.type == "subject":  # run on only this subject
        subject_id = destination.parents[
            "subject"
        ]  # project id will be found via subject_id
        session_only = False  # session_id will not be used to curate only the session
        log.info("Curating single subject, id %s", subject_id)
        config_file_name = CONFIG_FILE_NAME_PRE + "subject" + CONFIG_FILE_NAME_POST
        container = fw.get_subject(subject_id)

    elif destination.parent.type == "session":  # run on only this session
        log.info("Curating single session, id %s", session_id)
        config_file_name = CONFIG_FILE_NAME_PRE + "session" + CONFIG_FILE_NAME_POST
        container = fw.get_session(session_id)

    else:
        raise ValueError("Unexpected destination.parent.type!")

    still_needs_to_run = True

    # ___ Run what was just set up ---
    if use_saved_config:
        if project.get_file(config_file_name):
            still_needs_to_run = run_job_with_saved_config(
                fw,
                gear_context.config,
                project,
                container,
                config_file_name,
                destination.reload().job,
            )
        else:
            log.info("Did not find saved project configuration file")
    elif save_config:
        attach_config_to(
            gear_context, template, project, config_file_name, okay_to_use_saved_config,
        )

    if (
        still_needs_to_run and okay_to_use_saved_config
    ):  # don't run when use_or_save_config == "Disable Config File":

        main_with_args(
            api_key,
            session_id,
            reset,
            session_only,
            template_type,
            template_file,
            subject_id=subject_id,
            verbosity=verbosity,
            pickle_tree=pickle_tree,
            dry_run=dry_run,
        )

        group_id = destination.parents["group"]
        safe_group_label = make_file_name_safe(group_id, replace_str="_")

        safe_project_label = make_file_name_safe(project.label, replace_str="_")

        pre_path = f"output/{safe_group_label}_{safe_project_label}"

        bc = BIDSCuration()

        if session_only is False:
            session_id = (
                ""  # if this was set get_bids_info() will only return that session
            )
        num_subjects, num_sessions, num_duplicates = get_bids_info(
            project, bc, subject_id=subject_id, session_id=session_id
        )

        bc.save_niftis(pre_path)

        bc.save_intendedfors(pre_path, intended_for_regexes, fw)

        bc.save_acquisition_details(num_subjects, num_sessions, pre_path)

        # This must be called after save_acquisition_details() because it
        # sets bc.most_subjects_have
        bc.save_acquisitions(pre_path)

        if num_duplicates > 0:
            log.error("The following BIDS paths appear more than once:")
            for subject, paths in bc.all_seen_paths.items():
                for path, times_seen in paths.items():
                    if times_seen > 0:
                        log.info(f"  {path}")
            log.error(f"%s duplicate BIDS paths were detected", num_duplicates)

        else:
            log.info("No duplicates were found.")

    else:
        log.info("Did not curate")


if __name__ == "__main__":

    with flywheel.GearContext() as gear_context:
        main(gear_context)
