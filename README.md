# curate-bids
This is a Flywheel Gear that curates a Flywheel project according to the [BIDS Specification](https://bids-specification.readthedocs.io/en/stable/).  

This gear uses a "project curation template" to detect acquisition files and map them to the BIDS Specification. The base curation template is chosen as a configuration option.  The "ReproIn" template is recommended and it is the default.  This template assumes you used the [ReproIn naming convention](https://dbic-handbook.readthedocs.io/en/latest/mri/reproin.html) when setting up your protocol.  

Using the ReproIn template for projects containing subjects, sessions, and acquisitions named according to the ReproIn spec means that BIDS curation in Flywheel can proceed very close to automatically. The best place to start this is at the scanner console by setting the proper value for the `SeriesDescription` DICOM tag. 
> See this [Siemens Prisma 3T walkthrough](https://github.com/ReproNim/reproin/blob/master/docs/walkthrough-1.md) as an example of how to use the ReproIn naming convention.

This BIDS Curation V2 Gear is not backwards compatible with the previous version. The new gear will not look for a template file attached at the project level with a name ending in 'project-template.json'.  Instead, a custom template file can be provided as input to this gear (see below) and will be used to extend or override the base template.  If you have been running the V1 gear to curate your projects, please see the [CHANGES-V1-V2](CHANGES-V1-V2.md) description of breaking changes and improvements.

The version number of this gear has the form of major.minor.patch_major.minor.patch where the first triplet of numbers (before the underscore) refers to this gear and the second refers to the version of the [bids-client](https://gitlab.com/flywheel-io/public/bids-client).  Most of the funcionality of this gear is provided by the bids-client.

## Setup

BIDS curation is not difficult once a project is set up properly, however, setting everything up can take time. Proper setup requires attention to detail and some information about the project's scan protocol.  
> See [Flywheel + BIDS: how to start](https://docs.flywheel.io/hc/en-us/articles/360058755054-Flywheel-BIDS-how-to-start) for complete details on setting up and running BIDS curation.  

### Before you start
Before running this BIDS curation gear, you must first prepare your data with the following steps:
1. Run the [SciTran: DICOM MR Classifier](https://github.com/flywheel-apps/dicom-mr-classifier) gear on all the acquisitions in your dataset. This step extracts the DICOM header information and stores it as Flywheel Metadata.
1. Run the [DCM2NIIX: dcm2nii DICOM to NIfTI converter](https://github.com/flywheel-apps/dcm2niix) gear on all the acquisitions in your project.  This step generates the NIfTI files that BIDS Applications need from the DICOMS.  It also copies all Flywheel metadata from the DICOM to the NIfTI file (the DICOM header information we extracted in step 1).
1. Only then can you run this gear on the project.  

> More information about BIDS Curation on Flywheel can be found [here](https://docs.flywheel.io/hc/en-us/articles/360058755054-Flywheel-BIDS-how-to-start).  If you need to rename sessions or subjects before curation, you may find this gear helpful: [bids-pre-curate](https://github.com/flywheel-apps/bids-pre-curate).

### Running:
To run this gear, select a subject, session, or even the project.  To do this, find the "Run Analysis Gear" or "Run Gear" button for the desired level of the hierarchy.  If you run at the project level, it will curate the entire project.  Running at the subject level will curate all containers and files in that subject.  Running on an individual session will only curate that session.  If running at the subject or project level and some subjects or sessions have already been curated, see the description and warning about the "reset" configuration parameter below.  Detailed instructions on running this gear at a specific level are in "[Running Version 2.X](https://docs.flywheel.io/hc/en-us/articles/1500012005281-BIDS-curation-tutorial-part-2-running-the-BIDS-Curation-gear#h_01FJCEKPWE9WBBV81VKTSFZ8ZD)"

If you run this gear on a session by starting with the "Run Gear" button, be sure to select the "Analysis Gear" type, not "Utility Gear".  This new gear is an Analysis Gear and won't run properly as a Utility Gear.

### Inputs

#### template

This input is optional.  If provided, this JSON file will either extend a base template, or it can completely override all other templates.  If there is a top-level attribute of "extends", the value must be either "bids-v1", "reproin", or "default" and that base template will be extended.  If it does not have "extends", then the file must be a complete BIDS specification template, and it will be used all by itself.  The base templates are in the [BIDS Client repository](https://gitlab.com/flywheel-io/public/bids-client/-/tree/master/flywheel_bids/templates).

If a template is provided here as an input, the base_template configuration parameter is ignored.

### Config

![Inputs](README_img/Configuration.png)

#### base_template

Which base template to use. Options: ReproIn or BIDS-v1 (for backwards compatibility).  The default is ReproIn.

#### intendedfor_regexes

A list of pairs of regular expressions.  The first regex matches the field map acquisition label and the second matches the IntendedFor BIDS path.  Initial processing sets the list of IntendedFor paths to all files in the different 'anat', 'func', and 'dwi' BDIS folders.  These regex pairs should be used to filter that list so that only the appropriate IntendedFor paths remain.  Multiple pairs of regexes can be included.  All regexes should be separated by a single space.

#### reset

Remove all BIDS info from files before curating.  The default (unchecked) is to *not* reset the curation.

Warning: if you have manually selected any container or file to be ignored by checking the "bids ignore" flag in the BIDS Custom Information metadata, reset will remove that also, so you will need to do that again if it is important to you.  A more permanent way of ignoring files is to add "_ignore_BIDS" at the end of an acquisition container's label (the acquisition that contains the files to be ignored).  Note that this functionality depends on the project curation template: it must implement this feature by setting the ignore flag on the container (session or acquisition) to be ignored.

#### use_or_save_config

This option can be left un-set, or you can choose 'Save Config', 'Use Config', or 'Stop Using Config'.
If you choose 'Save Config', the current configuration will be saved at the current run level (on the session, subject, or project container) in a file called 'curate-bids-config.json', and it will be used by default on subsequent runs of this gear to set the configuration.  If you choose 'Use Config', that file will be ignored if present, and the currently set configuration will be used.  The default is to do neither, which will use the saved curate-bids-config.json file if it is present and to use the current configuration if not.  For example, using the 'Save Config' option one time while providing an input template, that template will be used by default for future runs.  If you choose 'Stop Using Config', the gear will disable the curation file and then quit without curating.

#### verbosity

This defaults to INFO but set it to DEBUG to see exactly what is happening to help with figuring out BIDS curation.

### Outputs

The main "outputs" of this gear are Custom Information *metadata* written to the "BIDS" namespace in all containers and files.  The gear does not actually create BIDS formatted data directly. That happens when exporting data in BIDS format using the CLI and also as the first step in running a BIDS Application gear.  The BIDS metadata controls if and how NIfTI files and json sidecars are created following the BIDS Specification.  Because of this, any changes you make by manually editing BIDS metadata will override what this gear has done.  See the note above about the "reset" option: if you re-run this gear with reset on, manual changes will need to be made again.

This gear will save a "report" on the state of BIDS curation in the form of a collection of .csv files.  Note that the gear may include a log message saying it was run successfully even though the BIDS curation might not have been properly completed.  The only way to know if curation was successful is to examine the .csv files in detail. See [BIDS curation tutorial part 5: interpreting the curation report](https://docs.flywheel.io/hc/en-us/articles/4405065304723-BIDS-curation-tutorial-part-4-interpreting-the-curation-report) to understand the information provided by the .csv files.
