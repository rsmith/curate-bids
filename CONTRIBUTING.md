# Contributing

This repository is set up so that testing is always run inside the test Docker container using scripts in the `./test/bin/` directory.

## Getting started

Follow instructions to [install pre-commit](https://pre-commit.com/#install) 

After cloning this repository, install pre-commit hooks (see __Linting and Testing__ below)

`pre-commit install`

Then build the main and the test Docker containers and run all tests using

`./test/bin/build-and-test.sh`

After the containers are built, you can use options `-B` (don't build this time) and `-s` 
(drop into shell).  This will allow you to iterate between editing code outside the 
container while running tests inside the container.  Start with

`./test/bin/build-and-test.sh -B -s`

and then run this inside the container:

`/src/tests/bin/tests.sh`

You can pass arguments to `pytest` by providing them after `--`:

`/src/tests/bin/tests.sh -- -v -k main`

The `-v` is for "verbose" and the `-k` runs specific tests using keyword expressions.

## Linting and Checking
Linting and other scripts are managed through [pre-commit](https://pre-commit.com/).  Pre-commit allows running hooks which can be defined locally, or in other 
repositories. These will be run on each commit:

* check-json: JSON syntax validator
* check-toml: TOML syntax validator (for pyproject.toml)
* pretty-format-json: Pretty print json
* no-commit-to-branch: Don't allow committing to master
* isort: Run isort
* black: Run black

These hooks can also be run manually or can be disabled.

### pre-commit usage:

* Run hooks manually:
    * Run on all files: `pre-commit run -a`
    * Run on certain files: `pre-commit run --files test/*`
* Update (e.g. clean and install) hooks: `pre-commit clean && pre-commit install`
* Disable all hooks: `pre-commit uninstall`
* Enable all hooks: `pre-commit install`
* Skip a hook on commit: `SKIP=<hook-name> git commit`
* Skip all hooks on commit: `git commit --no-verify`