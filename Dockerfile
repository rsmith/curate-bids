# curate-bids

FROM flywheel/bids-client:1.0.4

MAINTAINER Flywheel <support@flywheel.io>

# Make directory for flywheel spec (v0)
ENV FLYWHEEL /flywheel/v0
RUN mkdir -p ${FLYWHEEL}
COPY run.py ${FLYWHEEL}/run.py
COPY scripts ${FLYWHEEL}/scripts

COPY requirements.txt ${FLYWHEEL}
RUN pip install -r ${FLYWHEEL}/requirements.txt

# Set the command for local runs
CMD python3 /flywheel/v0/run.py
