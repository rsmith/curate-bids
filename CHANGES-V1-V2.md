# Changes from BIDS Curation V1 to V2

## Breaking Changes
The following are changes in the behavior of this gear that require the gear to be run
differently. See the [README](README.md) for information about how to run the gear.

### Project curation templates are now gear inputs
The V1 gear searched for project curation templates attached to the project container with
a specific name.  It was not obvious what happens if multiple templates were found.
This allowed a template to be used that was not associated with the gear run.  As inputs,
project curation templates cannot be deleted unless the gear run analysis is deleted.  This
better maintains provenance and makes it obvious that the template was used.

### Gear runs at session, subject, or project level
The configuration parameter "entire_project" has been removed.  That allowed the gear to be
run on a session but curate the entire project.  It made it difficult to locate where the
job was run. Now, the gear can be run at the session, subject, or project level of the hierarchy.
This makes clear what part(s) of the project have been curated in each job run.

### Gear output is a "curation report"
The V1 gear was a utility gear that modified metadata to do curation, but it did not
produce any "output". The V2 gear is now an analysis gear.  It now produces a "curation
report" (a collection of .cvs files) that describe the state of curation.  Be sure to
run the new gear from the "Analysis Gear" menu, not the "Utility Gear" menu.

## Other Changes

The following are improvements to the gear.

### Base project curation templates are now ReproIn and BIDS-v1
Two base project curation templates are present in the gear and can be selected in the
configuration.  The default is ReproIn.  The BIDS-v1 template is included for backwards
compatibility.

### IntendedFor Regular Expressions
As in the V1 gear, the project curation template can be modified to better handle field map
"IntendedFor" lists.  The V2 gear provides an additional mechanism to filter this list using
regular expressions provided in the configuration.

### New configuration option: use_or_save_config
This new option was added to make running the V2 gear more like the V1 gear.  By saving the
configuration (including an input template), future runs of the gear can use the saved 
configuration by merely pressing the "Run Analysis Gear" button.

### The gear now produces much better logging messages
Which is nice.  So you've got that going for you.  But you still have to check the .csv files
to see how the curation went.  Just because the curation gear ran successfully, it doesn't mean
that everything was curated properly.
